import axios from 'axios';

const API_KEY = 'f67cc6056b3c57c0fcc7ed48e3bbc140';
const units = 'metric'

// Use variables instead of strings for action.type i order to avoid typos
export const SEARCH_CURRENT_WEATHER = 'SEARCH_CURRENT_WEATHER';
export const SELECT_CITY = 'SELECT_CITY';
export const CLEAR_SELECT_CITY = 'CLEAR_SELECT_CITY';
export const GET_WEATHER_FORECAST = 'OPEN_WEATHER_FORECAST'
export const CLEAR_FORECAST = 'CLEAR_FORECAST'

export function getCurrentWeather(city){
    const URL_ROOT = `http://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}`;
    const url = `${URL_ROOT}&q=${city}&units=${units}`;
    const promise = axios.get(url);
    return {
        type : SEARCH_CURRENT_WEATHER,
        payload : promise
    }
}
export function getForecast(city){
    const URL_ROOT = `http://api.openweathermap.org/data/2.5/forecast/daily?appid=${API_KEY}`;
    const url = `${URL_ROOT}&q=${city}&cnt=7&units=${units}`;
    const promise = axios.get(url);
    return {
        type : GET_WEATHER_FORECAST,
        payload : promise
    }
}
export function clearForecast(){
    return {
        type : CLEAR_FORECAST,
        payload : null
    }
}
export function selectCity(city){
    return {
        type : SELECT_CITY,
        payload : city
    }
}
export function clearSelectedCity(){
    return {
        type : CLEAR_SELECT_CITY,
        payload : null
    }
}
