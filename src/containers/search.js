import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCurrentWeather, clearSelectedCity, clearForecast } from '../actions/index';


class Search extends Component{
    constructor(props){
        super(props);
        this.state = {
            term : ''
        };
    }
    updateTerm(term){
        this.setState({
            term : term
        })
    }
    onFormSubmit(event){
        event.preventDefault();
        this.props.getCurrentWeather(this.state.term);
        this.props.clearForecast();
        setTimeout(this.props.clearSelectedCity, 400);
        this.setState({
            term : ''
        })
    }
    render() {
        return (
            <nav className="navbar navbar-default" role="navigation">
                <form className="input-group" onSubmit={(event) => this.onFormSubmit(event)}>
                    <input type="text"
                        onChange={(event) => this.updateTerm(event.target.value)}
                        value={this.state.term}
                        className="form-control"
                        placeholder="See current weather in your city"/>
                    <div className="input-group-btn">
                        <button className="btn btn-default">Search</button>
                    </div>
                </form>
            </nav>
        )
    } 
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getCurrentWeather, clearSelectedCity, clearForecast}, dispatch);
}

export default connect(null, mapDispatchToProps)(Search);