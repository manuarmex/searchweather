import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectCity, getForecast, clearForecast } from '../actions/index';
import SelectedCity from '../components/selected-city';
import LastCitiesList from '../components/last-cities-list';
import Forecast from '../components/forecast';

class WeatherDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
            forecastAktive :false 
        }
        // forecastAktive is in charge of displaying/hiding forecast visibility buttons
    }
    onOpenForecast() {
        this.setState({
            forecastAktive : true
        })
        const city = this.currentCityToDisplay();
        this.props.getForecast(city.name);
    }
    onCloseForecast() {
        this.setState({
            forecastAktive : false
        })
        this.props.clearForecast();
    }
    currentCityToDisplay(){
        // Get city to displat from the top of the searched list if no city has been selected by user 
        const cityToReturn = this.props.selectedCity? this.props.selectedCity : this.props.weatherList[0];
        return cityToReturn;
    }
    onSelectCity(city){
        this.props.selectCity(city);
        this.props.clearForecast();
        this.setState({
            forecastAktive : false
        })
    }
    render() {
        return (
            <div className="container">
                <main>
                    <SelectedCity 
                        city={this.currentCityToDisplay()}>
                    {!this.state.forecastAktive && <div className="btn btn-default" onClick={() => this.onOpenForecast()}>See Forecast</div>}
                    {this.state.forecastAktive && <div className="btn btn-default" onClick={() => this.onCloseForecast()}>Hide Forecast</div>}
                    </SelectedCity>
                    <Forecast 
                        forecastData={this.props.forecast}/>
                </main>
                <aside>
                    <LastCitiesList 
                        listOfCities={this.props.weatherList}
                        selectCity = {(city) => this.onSelectCity(city)}/>
                </aside>
            </div>
        );
    }
}

function mapStateToProps(state){
    return { 
        weatherList : state.weatherList,
        selectedCity : state.selectedCity,
        forecast : state.forecast
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({selectCity, getForecast, clearForecast}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherDisplay);