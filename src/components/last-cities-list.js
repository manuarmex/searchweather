import React, {Component} from 'react';

class LastCitiesList extends Component {
    renderCitieslist(city){
         const flag =`http://openweathermap.org/images/flags/${city.sys.country.toLowerCase()}.png`;
        return (
            <li key={city.name}  onClick={() => this.props.selectCity(city)}> 
                {city.name}, {city.sys.country} <img className="flag" src={flag} alt={city.sys.country}/>
            </li> 
        )
    }
    render() {
        if (this.props.listOfCities.length === 0){
            return <div></div>
        }
        return (
            <div className="last-cities">
                <div className="title">Last searched cities</div>
                <ul>
                    {this.props.listOfCities.map((city) => this.renderCitieslist(city))}
                </ul>
            </div>
        );
    }
}
LastCitiesList.propTypes = {
    listOfCities : React.PropTypes.array,
    selectCity : React.PropTypes.func
}

export default LastCitiesList;