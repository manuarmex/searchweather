import React, { Component } from 'react';
import Search from '../containers/search';
import WeathersDisplay from '../containers/weathers-display';

export default class App extends Component {
  render() {
    return (
      <div>
          <Search />
          <WeathersDisplay />
      </div>
    );
  }
}
