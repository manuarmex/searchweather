import React, {Component} from 'react';

class SelectedCity extends Component {
    
    render() {
       
        if (!this.props.city){
            return <div></div>
        }
        const icon =`http://openweathermap.org/img/w/${this.props.city.weather[0].icon}.png`;
        const flag =`http://openweathermap.org/images/flags/${this.props.city.sys.country.toLowerCase()}.png`;
        return (
            <div className="selected-city">
                <ul>
                    <li className="name">Weather in {this.props.city.name}, {this.props.city.sys.country} <img  className="flag" src={flag} alt={this.props.city.sys.country}/></li>
                    <li className="temp"> <img src={icon} alt=""/>{this.props.city.main.temp} °C</li>
                    <li><span className="title">Cloudiness:</span> {this.props.city.clouds.all}%</li>
                    <li><span className="title">Humidity:</span> {this.props.city.main.humidity}% </li>
                </ul>
                {this.props.children}
            </div>
        );
    }
}

SelectedCity.propTypes = {
    city : React.PropTypes.object
}

export default SelectedCity;