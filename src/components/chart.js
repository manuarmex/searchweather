import * as d3 from 'd3';
import React, {Component} from 'react';
import ReactFauxDOM from 'react-faux-dom';
import {TEMPERATURE_TYPE, RAIN_TYPE, CLOUDS_TYPE} from './forecast'

class Chart extends Component {
 
  render () {
   
    const dataType = this.props.activeChart;
    const forecastData = this.props.data;
   
     // Create a element.
    const el = ReactFauxDOM.createElement('div')

    // Create SVG and positionate applying margins to the dimentions
    const margin = { top: 10, right: 20, bottom: 30, left: 30 };
    const width = this.props.width - margin.left - margin.right;
    const height = this.props.height - margin.top - margin.bottom;

    const svg = d3.select(el)
    .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
    .append('g')
        .attr('transform', `translate(${margin.left}, ${margin.top})`);

    // Create the X axis for the bottom
    var xScale = d3.scaleBand()
        .padding(0.4)
        .domain(forecastData.map(d => d.date))
        .range([0, width]);

    var xAxis = d3.axisBottom(xScale);
    svg
        .append('g')
        .attr('transform', `translate(0, ${height})`)
        .call(xAxis)
        .selectAll('text')
        .attr('transform', `translate(15, 0)`)
        .style('text-anchor', 'end');
    
    // Create the Y axis 
    // Different domains for every typo of data to render
    let yDomain = [0,100]
    if (dataType === TEMPERATURE_TYPE){
        yDomain = [
            d3.min(forecastData, d => d[dataType]) - 5,
            d3.max(forecastData, d => d[dataType]) + 5
        ]
    }
    if (dataType === RAIN_TYPE){
        yDomain = [
            d3.min(forecastData, d => d[dataType]),
            d3.max(forecastData, d => d[dataType]) + 5
        ]
    }
    const yScale = d3.scaleLinear()
        .domain(yDomain)
        .range([height, 0]);
    svg
        .append('g')
        .call(d3.axisLeft(yScale));

    // Create the bars displaying data
    svg.selectAll('rect')
        .data(forecastData)
        .enter()
        .append('rect')
        .attr('x', d => xScale(d.date))
        .attr('y', d => yScale(d[dataType]))
        .attr('width', d => xScale.bandwidth())
        .attr('height', d => height - yScale(d[dataType]))
        .style('fill', '#06befb');
    
    return el.toReact()
  }
}

export default Chart