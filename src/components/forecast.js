import React, {Component} from 'react';
import Chart from './chart'

// Use variables instead of strings for chart datatype i order to avoid typos in future coding
// since it is going to be used across components (both in forecast and chart components)
export const TEMPERATURE_TYPE = 'temperature'; 
export const RAIN_TYPE = 'rain';    
export const CLOUDS_TYPE = 'clouds'; 

class Forecast extends Component{
    constructor(props){
        super(props)
        this.state = {
            dataToShowInChart : TEMPERATURE_TYPE
        }
    }
    onChangeChart(type){
        this.setState({
            dataToShowInChart : type
        })
    }
    getMapData(data){
        const newData = data.map((day) => {
                return {
                        'date': new Date(day.dt*1000),
                        'clouds' : day.clouds || 0,
                        'temperature' : day.temp.day,
                        'rain' : day.rain || 0
                    }
                })  
        newData.forEach(day => {
            day.date = day.date.toISOString().slice(5,10)
        });
        
        return newData;
    }
    getDescription(){
        let type = this.state.dataToShowInChart;
        if (type === RAIN_TYPE){
            return 'Rain in mm'
        }
        if (type === CLOUDS_TYPE){
            return 'Cloud coverage in %'
        }
        if (type === TEMPERATURE_TYPE){
            return 'Temperature in °C'
        }
    }
    render(){
        if (!this.props.forecastData){
            return <div></div>
        }
        return (
            <div className="forecast">
                <p >Next 7 days</p>
                <p className="description">( {this.getDescription() } )</p>
                <Chart
                    width={370}
                    height={210}
                    data={this.getMapData(this.props.forecastData.list)}
                    activeChart = {this.state.dataToShowInChart}
                />
                <div className="buttons">
                    <div className="btn btn-default" onClick={(type) => this.onChangeChart('temperature')}>Temp</div>
                    <div className="btn btn-default" onClick={(type) => this.onChangeChart('rain')}>Rain</div>
                    <div className="btn btn-default" onClick={(type) => this.onChangeChart('clouds')}>Clouds</div>
                </div>
            </div>
        );
    }
}

Forecast.propTypes = {
    forecastData : React.PropTypes.object
}

export default Forecast;