import { SELECT_CITY, CLEAR_SELECT_CITY  } from '../actions/index.js';

export default function(state = null, action){
    switch (action.type){
        case SELECT_CITY :
            return action.payload
        case CLEAR_SELECT_CITY:
            return null;
    }
    return state
}