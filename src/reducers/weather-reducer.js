import { SEARCH_CURRENT_WEATHER } from '../actions/index.js';

function deleteDuplicatesObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].name === obj.name) {
            list.splice(i,1);
        }
    }
}

export default function(state = [], action) {
    switch (action.type) {
        case  SEARCH_CURRENT_WEATHER:
            if (!action.error) {
                let stateCopy = state;
                
                // Check for duplicate and remove it before inserting the new search
                if (stateCopy.length > 0){
                    deleteDuplicatesObject(action.payload.data, stateCopy)
                }

                // add new element to the top of the list
                stateCopy = [action.payload.data, ...stateCopy]
                
                // and delete last item is the list has more than 5 elements     
                if (stateCopy.length > 5){
                    stateCopy.pop();
                }
                return stateCopy
            }
    }
    return state;
} 