import { combineReducers } from 'redux';
import WeatherReducer from './weather-reducer';
import SelectedCityReducer from './selected_city-reducer';
import GetForecast from './forecast-reducer';

const rootReducer = combineReducers({
  weatherList : WeatherReducer,
  selectedCity : SelectedCityReducer,
  forecast : GetForecast
});

export default rootReducer;
