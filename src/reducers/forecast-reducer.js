import { GET_WEATHER_FORECAST } from '../actions/index.js';
import { CLEAR_FORECAST } from '../actions/index.js';

export default function(state = null, action) {
    switch (action.type) {
        case  GET_WEATHER_FORECAST:
            if (!action.error){
                return action.payload.data
            }
        case  CLEAR_FORECAST:
            if (!action.error){
                return null
            }
    }
    return state;
} 