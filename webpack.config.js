const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: path.resolve(__dirname,'/'),
    filename: 'bundle.js'
  },
  devServer: {
      inline : true,
      port : 3333,
      historyApiFallback: true,
      contentBase: './'
  },
  devtool: "source-map",
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel'
    },
    {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};
