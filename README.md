# Search weather app

Simple app to search weather in cities developed using React and Redux and D3.js

### Functionality ###

* Search weather using city name 
* Get the last 5 searched cities
* See forecast for the next 7 days

### Run it locally ###

- npm install
- npm run start

served in localhost:3333

#### Demo

You can [see a demo here](http://arcemedia.no/searchWeather/)